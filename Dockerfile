FROM ubuntu:20.04

WORKDIR /app

COPY . /app
COPY ./tools /app

RUN apt-get -y update && \
    apt-get install -y python3-pip && \
    python3 -m pip install numpy && \
    python3 -m pip install scipy && \
    python3 -m pip install matplotlib

ENV PATH="/app/tools:$PATH"    

CMD ["bash"]
